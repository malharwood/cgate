@echo off
REM command file to build and publish dockerfiles

SET ver=2.11.5
SET zip=cgate-2.11.5_3260

echo Building v%ver%

set arg1=%1

REM Set all flags to false
set cgate=false
set cgatearm=false

REM If we get no arguments, set all to true
IF "%arg1%"=="" (
  echo No option selected!
)

REM if first argument is known, then use it
REM NOTE we do not currently support multiple arguments!
IF "%arg1%"=="all" (
  set cgate=true
  set cgatearm=true
  echo Building all!
)
IF "%arg1%"=="cgate" (set cgate=true)
IF "%arg1%"=="cgatearm" (set cgatearm=true)

    if "%cgate%" NEQ "true" (
      if "%cgatearm%" NEQ "true" (
          echo You havent selected a valid option.
          echo Options are:
          echo - cgate
          echo - cgatearm
          echo - all for all .. 
          GOTO:eof
      )
    )

echo Logging Into Gitlab.com
docker login registry.gitlab.com

echo Cleaning up old images
docker system prune -a -f

echo Downloading CGATE zip
rem curl -O https://updates.clipsal.com/ClipsalSoftwareDownload/mainsite/cis/technical/CGate/cgate-%ver%.zip
curl -O "C-Gate-V%ver2%.zip" "https://download.schneider-electric.com/files?p_Archive_Name=C-Gate-V%ver2%.zip&p_enDocType=Software+-+Release&p_Doc_Ref=C-Gate-V%ver2%"
rem curl -O https://download.schneider-electric.com/files?p_Archive_Name=C-Gate-V2115.zip&p_enDocType=Software+-+Release&p_Doc_Ref=C-Gate-V2115

tar -xf %zip%.zip
rem del /q c-gate-%ver2%.zip
del /q cgate\tag\*
del /q cgate\config\access.txt

if "%cgate%" == "true" (
  echo Building cgate
  docker build -t registry.gitlab.com/malharwood/cgate:%ver% -f Dockerfile.amd64 .
  docker tag registry.gitlab.com/malharwood/cgate:%ver% registry.gitlab.com/malharwood/cgate:latest
  docker push registry.gitlab.com/malharwood/cgate --all-tags
  
  malharwood/cgate:
)

if "%cgatearm%" == "true" (
  echo Building cgate ARM
  docker build -t registry.gitlab.com/malharwood/cgate:arm-%ver% -f Dockerfile.arm .
  docker tag registry.gitlab.com/malharwood/cgate:arm-%ver% registry.gitlab.com/malharwood/cgate:arm-latest

  docker push registry.gitlab.com/malharwood/cgate --all-tags
)

rmdir /s /q cgate

echo Cleaning up images
docker system prune -a -f

